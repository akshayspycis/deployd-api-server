import {DefaultCrudRepository} from '@loopback/repository';
import {Userdetails, UserdetailsRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UserdetailsRepository extends DefaultCrudRepository<
  Userdetails,
  typeof Userdetails.prototype.id,
  UserdetailsRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Userdetails, dataSource);
  }
}
