import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Userdetails} from '../models';
import {UserdetailsRepository} from '../repositories';

export class UserdetailsController {
  constructor(
    @repository(UserdetailsRepository)
    public userdetailsRepository : UserdetailsRepository,
  ) {}

  @post('/userdetails', {
    responses: {
      '200': {
        description: 'Userdetails model instance',
        content: {'application/json': {schema: getModelSchemaRef(Userdetails)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Userdetails, {exclude: ['id']}),
        },
      },
    })
    userdetails: Omit<Userdetails, 'id'>,
  ): Promise<Userdetails> {
    return this.userdetailsRepository.create(userdetails);
  }

  @get('/userdetails/count', {
    responses: {
      '200': {
        description: 'Userdetails model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Userdetails)) where?: Where<Userdetails>,
  ): Promise<Count> {
    return this.userdetailsRepository.count(where);
  }

  @get('/userdetails', {
    responses: {
      '200': {
        description: 'Array of Userdetails model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Userdetails)},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Userdetails)) filter?: Filter<Userdetails>,
  ): Promise<Userdetails[]> {
    return this.userdetailsRepository.find(filter);
  }

  @patch('/userdetails', {
    responses: {
      '200': {
        description: 'Userdetails PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Userdetails, {partial: true}),
        },
      },
    })
    userdetails: Userdetails,
    @param.query.object('where', getWhereSchemaFor(Userdetails)) where?: Where<Userdetails>,
  ): Promise<Count> {
    return this.userdetailsRepository.updateAll(userdetails, where);
  }

  @get('/userdetails/{id}', {
    responses: {
      '200': {
        description: 'Userdetails model instance',
        content: {'application/json': {schema: getModelSchemaRef(Userdetails)}},
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Userdetails> {
    return this.userdetailsRepository.findById(id);
  }

  @patch('/userdetails/{id}', {
    responses: {
      '204': {
        description: 'Userdetails PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Userdetails, {partial: true}),
        },
      },
    })
    userdetails: Userdetails,
  ): Promise<void> {
    await this.userdetailsRepository.updateById(id, userdetails);
  }

  @put('/userdetails/{id}', {
    responses: {
      '204': {
        description: 'Userdetails PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() userdetails: Userdetails,
  ): Promise<void> {
    await this.userdetailsRepository.replaceById(id, userdetails);
  }

  @del('/userdetails/{id}', {
    responses: {
      '204': {
        description: 'Userdetails DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.userdetailsRepository.deleteById(id);
  }
}
